#!/bin/sh

set -e

find . -name '*.cpp' -o -name '*.h' | egrep -v '(hippomocks|hpp|moc_)' | xargs clang-format -i
git diff --exit-code
