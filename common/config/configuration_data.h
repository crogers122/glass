//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef CONFIGURATION_DATA__H
#define CONFIGURATION_DATA__H

#include <configuration_datum.h>
#include <unordered_map>
class configuration_data
{
public:
    configuration_data(string_type set_name) : m_set_name(set_name){};
    virtual ~configuration_data(){};

    void add_config_datum(b_config_datum &datum)
    {
        m_data_set.insert({datum.name(), datum});
        m_key_list.push_back(datum.name());
    }

    b_config_datum &get_config_datum(string_type key)
    {
        return m_data_set.at(key);
    }

    std::list<string_type> key_list(void)
    {
        return m_key_list;
    }

protected:
    std::unordered_map<string_type, b_config_datum &> m_data_set;
    std::list<string_type> m_key_list;
    string_type m_set_name;
};

#endif // CONFIGURATION_DATUM__H
