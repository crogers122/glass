#ifndef LOGGING_H
#define LOGGING_H

//
// Logging
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <QObject>
#include <QtGlobal>
#include <stdio.h>  // fprintf, fflush
#include <syslog.h> // openlog, syslog

// ============================================================================
// Definitions
// ============================================================================

class Logging : public QObject
{
    Q_OBJECT

public:
    Logging() : debugMode(false), syslogMode(false)
    {
        openlog(nullptr, LOG_PID, LOG_DAEMON);
    }

    static void logOutput(QtMsgType type, const QMessageLogContext &, const QString &msg)
    {
        Logging *logger = Logging::logger();

        switch (type) {
            case QtDebugMsg:
                if (logger && logger->debugMode) {
                    if (logger && logger->syslogMode) {
                        syslog(LOG_INFO, "[DEBUG] %s\n", qPrintable(msg));
                    } else {
                        fprintf(stderr, "[DEBUG] %s\n", qPrintable(msg));
                        fflush(stderr);
                    }
                }
                break;
#if QT_VERSION >= 0x050500
            case QtInfoMsg:
                if (logger && logger->syslogMode) {
                    syslog(LOG_INFO, "[INFO] %s\n", qPrintable(msg));
                } else {
                    fprintf(stderr, "[INFO] %s\n", qPrintable(msg));
                    fflush(stderr);
                }
                break;
#endif
            case QtWarningMsg:
                if (logger && logger->syslogMode) {
                    syslog(LOG_WARNING, "[WARNING] %s\n", qPrintable(msg));
                } else {
                    fprintf(stderr, "[WARNING] %s\n", qPrintable(msg));
                    fflush(stderr);
                }
                break;
            case QtCriticalMsg:
                if (logger && logger->syslogMode) {
                    syslog(LOG_CRIT, "[CRITICAL] %s\n", qPrintable(msg));
                } else {
                    fprintf(stderr, "[CRITICAL] %s\n", qPrintable(msg));
                    fflush(stderr);
                }
                break;
            case QtFatalMsg:
                if (logger && logger->syslogMode) {
                    syslog(LOG_ALERT, "[FATAL] %s\n", qPrintable(msg));
                } else {
                    fprintf(stderr, "[FATAL] %s\n", qPrintable(msg));
                    fflush(stderr);
                }
                break;
        }
    }

    static Logging *logger()
    {
        static Logging *myInstance = nullptr;

        if (!myInstance) {
            myInstance = new Logging();
        }

        if (!myInstance) {
            fprintf(stderr, "[CRITICAL] Logger could not be instantiated!\n");
            fflush(stderr);
        }

        return myInstance;
    }

    bool debugMode;
    bool syslogMode;
};

#endif // LOGGING_H
