//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef BRIGHTNESS_ACTION__H
#define BRIGHTNESS_ACTION__H

#include <inputaction.h>
#include <string>

class brightness_action_t : public input_action_t
{
    Q_OBJECT

public:
    explicit brightness_action_t(const std::string &direction);
    ~brightness_action_t(void);

    void operator()();

signals:
    void increase_brightness();
    void decrease_brightness();

private:
    bool m_increase;
};

#endif //BRIGHTNESS_ACTION__H
