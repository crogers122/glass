//
// Glass Display
//
// Copyright (C) 2016 - 2020 Assured Information Security, Inc. All rights reserved.
//
#ifndef GESTURE_FILTER__H
#define GESTURE_FILTER__H

#include <QMap>
#include <inputserverfilter.h>

enum Type {
    TYPE_ERROR = 0,
    TYPE_SWIPE,
    TYPE_EDGE_SWIPE,
    TYPE_TAP_AND_HOLD,
    TYPE_DOUBLE_TAP
};

enum Direction {
    DIRECTION_ERROR = 0,
    DIRECTION_LEFT,
    DIRECTION_RIGHT,
    DIRECTION_UP,
    DIRECTION_DOWN
};

enum Edge {
    EDGE_ERROR = 0,
    EDGE_LEFT,
    EDGE_RIGHT,
    EDGE_TOP,
    EDGE_BOTTOM
};

struct Gesture {
    struct Trigger {
        Type type;
        int32_t finger_count;
        union {
            Direction direction;
            Edge edge;
        };

    } trigger;

    int32_t touch_count;
    QMap<int32_t, QList<point_t>> slot_to_signal;
    bool entered;
    bool fired;
};

class gesture_filter_t : public input_server_filter_t
{
    Q_OBJECT

public:
    gesture_filter_t(window_manager_t &wm,
                     std::shared_ptr<input_action_t> touch_count_action,
                     std::shared_ptr<input_action_t> gesture_action,
                     std::shared_ptr<input_action_t> untouch_count_action);
    virtual ~gesture_filter_t(void);

    virtual bool filter_event(std::shared_ptr<vm_input_t> &vm, xt_input_event *event);
    virtual bool set_triggers(json triggers);
    virtual bool clear_triggers(void);
    virtual bool add_trigger(json trigger);

signals:

    void filter_fire(const std::shared_ptr<input_action_t> &action);

private:
    QPair<point_t, point_t> generate_signal_vector(struct Gesture &gesture);
    int32_t distance_threshold(const struct Gesture &gegsture);

    uint32_t m_touch_count{0};
    uint32_t m_direction{0};
    QMap<int32_t, QList<QPoint>> m_slot_to_signal;
    window_manager_t &m_wm;
    std::shared_ptr<input_action_t> m_touch_count_action;
    std::shared_ptr<input_action_t> m_gesture_action;
    std::shared_ptr<input_action_t> m_untouch_count_action;

    static const int32_t vector_size;
    static const int32_t granularity;
    static const int32_t direction_threshold;
    static const int32_t inverse_direction_threshold;
    QList<struct Gesture> m_triggers;
};

#endif // GESTURE_FILTER__H
