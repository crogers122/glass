//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <libinputsource.h>

const float touch_scroll_sensitivity = 0.08;

extern "C" {
static const char *seat = "seat0";

static int
open_restricted(const char *path, int flags, void *user_data)
{
    (void) user_data;
    int fd = open(path, flags);

    if (fd >= 0 && ioctl(fd, EVIOCGRAB, 1) < 0) {
        qWarning() << "Grabbing the input fd has failed!!!";
    }

    return fd < 0 ? -errno : fd;
}

static void
close_restricted(int fd, void *user_data)
{
    (void) user_data;

    if (fd >= 0 && ioctl(fd, EVIOCGRAB, 0) < 0) {
        qWarning() << "Ungrabbing the input fd has failed!!!";
    }

    close(fd);
}

static const struct libinput_interface interface =
    {
        open_restricted,
        close_restricted,
};

#ifdef TAP_INPUT
void
libinput_source_t::write_event(uint16_t type, uint16_t code, int32_t value)
{
    struct input_event ev;

    if (tap_input_enabled) {
        memset(&ev, 0, sizeof(struct input_event));
        gettimeofday(&ev.time, NULL);
        ev.type = type;
        ev.code = code;
        ev.value = value;
        write(m_tap_fd, &ev, sizeof(struct input_event));
    }
}

void
libinput_source_t::write_sync_event()
{
    struct input_event ev;

    if (tap_input_enabled) {
        memset(&ev, 0, sizeof(struct input_event));
        gettimeofday(&ev.time, NULL);
        ev.type = EV_SYN;
        ev.code = 0;
        ev.value = 0;
        write(m_tap_fd, &ev, sizeof(struct input_event));
    }
}
#endif
}

libinput_source_t::libinput_source_t(window_manager_t &wm) : m_libudev_ctx(udev_new()), m_wm(wm)
{
    // Creates a new libinputCtx struct from udev.
    m_libinput_ctx = libinput_udev_create_context(&interface, NULL, m_libudev_ctx);

    // Make sure we have a valid context
    Q_CHECK_PTR(m_libinput_ctx);

    // Assign a seat ID to the libinputCtx
    if (libinput_udev_assign_seat(m_libinput_ctx, seat))
        throw std::runtime_error("Failed to assign a new seat ID to libinput context.");

    // Libinput keeps a single fd for all events. This is the fd used to notify pending events.
    m_input_fd = libinput_get_fd(m_libinput_ctx);

    // Make sure we have valid fd
    Q_ASSERT(m_input_fd >= 0);

    m_input_socket = std::make_unique<QSocketNotifier>(m_input_fd, QSocketNotifier::Read);
    QObject::connect(m_input_socket.get(), &QSocketNotifier::activated, this, &libinput_source_t::libinput_socket_event);
    QObject::connect(this, &libinput_source_t::input_event, this, &libinput_source_t::receive_event);

#ifdef TAP_INPUT
    m_tap_fd = open("/tmp/tap_input", O_WRONLY | O_CREAT | O_TRUNC);
    tap_input_enabled = false;
#endif
}

libinput_source_t::~libinput_source_t(void)
{
    ::close(m_input_fd);

    // Suspend monitoring of new devices, and close existing ones.
    libinput_suspend(m_libinput_ctx);
}

void
libinput_source_t::libinput_socket_event(int socket)
{
    (void) socket;
    libinput_event *ev = NULL;

    // Validate out libinput context pointer
    if (!m_libinput_ctx)
        return;

    // Reads events on the fd and processes them internally.
    libinput_dispatch(m_libinput_ctx);

    // Gets the next event from libinputs internal event queue.
    // Keep processing events until there are none left.
    while ((ev = libinput_get_event(m_libinput_ctx))) {

        /*
        We don't handle the actual events within this thread.
        Try to keep this simple, and just dispatch these events
        to be handled elsewhere.

        This may be some bad juju, as the event is allocated in
        this thread, then destroy in the main thread. Have to do
        some testing to see if this is an issue.
        */
        emit input_event(ev);
    }
}

void
libinput_source_t::set_led_code(const uint32_t led_code)
{
    for (auto &dev : m_devices) {
        if (libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_KEYBOARD) != 0) {
            uint32_t leds = 0;

            if ((led_code & LED_CODE_SCROLLLOCK) == LED_CODE_SCROLLLOCK)
                leds |= LIBINPUT_LED_SCROLL_LOCK;

            if ((led_code & LED_CODE_NUMLOCK) == LED_CODE_NUMLOCK)
                leds |= LIBINPUT_LED_NUM_LOCK;

            if ((led_code & LED_CODE_CAPSLOCK) == LED_CODE_CAPSLOCK)
                leds |= LIBINPUT_LED_CAPS_LOCK;

            libinput_device_led_update(dev, (libinput_led) leds);
        }
    }
}

void
libinput_source_t::receive_event(void *event)
{
    Q_CHECK_PTR(event);

    struct libinput_event *ev = (struct libinput_event *) event;
    Q_CHECK_PTR(ev);

    switch (libinput_event_get_type(ev)) {

        case LIBINPUT_EVENT_NONE:
            break;

        case LIBINPUT_EVENT_DEVICE_ADDED: {
            struct libinput_device *dev = libinput_event_get_device(ev);
            struct libinput_seat *li_seat = libinput_device_get_seat(dev);

            Q_CHECK_PTR(dev);
            Q_CHECK_PTR(li_seat);

            if (!m_devices.contains(dev)) {
                m_devices.append(dev);
            }

            libinput_device_ref(dev);

            //check if device has pointer cap
            if (libinput_device_has_capability(dev, LIBINPUT_DEVICE_CAP_POINTER) != 0) {
                emit add_pointer_device();
            }

            //check if it is multitouch
            if (libinput_device_has_capability(dev, LIBINPUT_DEVICE_CAP_TOUCH) != 0) {
                emit add_multitouch_device();
            }

            break;
        }

        case LIBINPUT_EVENT_DEVICE_REMOVED: {
            struct libinput_device *dev = libinput_event_get_device(ev);
            struct libinput_seat *li_seat = libinput_device_get_seat(dev);

            Q_CHECK_PTR(dev);
            Q_CHECK_PTR(li_seat);

            libinput_device_unref(dev);
            m_devices.removeOne(dev);

            break;
        }

        case LIBINPUT_EVENT_KEYBOARD_KEY:
            handle_key_event(ev);
            break;

        case LIBINPUT_EVENT_POINTER_MOTION:
            // Relative mouse movement
            handle_relative_mouse_event(ev);
            break;

        case LIBINPUT_EVENT_POINTER_MOTION_ABSOLUTE:
            // Absolute mouse movement
            handle_absolute_mouse_event(ev);
            break;

        case LIBINPUT_EVENT_POINTER_BUTTON:
            // Mouse button
            handle_mouse_button_event(ev);
            break;

        case LIBINPUT_EVENT_POINTER_AXIS:
            // Scrollwheel
            handle_scroll_event(ev);
            break;

        //multitouch
        case LIBINPUT_EVENT_TOUCH_DOWN:
        case LIBINPUT_EVENT_TOUCH_MOTION:
        case LIBINPUT_EVENT_TOUCH_UP:
        case LIBINPUT_EVENT_TOUCH_CANCEL:
        case LIBINPUT_EVENT_TOUCH_FRAME:
            handle_multitouch_event(ev);
            break;
        //tablet
        case LIBINPUT_EVENT_TABLET_TOOL_AXIS:
        case LIBINPUT_EVENT_TABLET_TOOL_PROXIMITY:
        case LIBINPUT_EVENT_TABLET_TOOL_TIP:
        case LIBINPUT_EVENT_TABLET_TOOL_BUTTON:
            handle_tablet_tool_event(ev);
            break;

        default:
            qWarning() << "Unhandled libinput event";
            break;
    }

    libinput_event_destroy(ev);
}

void
libinput_source_t::populate_absolute_event(struct xt_input_event *event, struct libinput_event *input_event)
{
    rect_t input_rect = m_wm.input_plane()->rect();
    int32_t input_width = input_rect.width();
    int32_t input_height = input_rect.height();

    if (!event || !input_event) {
        return;
    }

    // Get the X and Y position of the eventc
    if (libinput_event_get_type(input_event) == LIBINPUT_EVENT_POINTER_MOTION_ABSOLUTE) {
        struct libinput_event_pointer *point = libinput_event_get_pointer_event(input_event);

        if (!point) {
            return;
        }

        event->data.absolute.absX = libinput_event_pointer_get_absolute_x(point);
        event->data.absolute.absY = libinput_event_pointer_get_absolute_y(point);

    } else if (libinput_event_get_type(input_event) == LIBINPUT_EVENT_TOUCH_DOWN ||
               libinput_event_get_type(input_event) == LIBINPUT_EVENT_TOUCH_MOTION) {

        struct libinput_event_touch *touch = libinput_event_get_touch_event(input_event);

        if (!touch) {
            return;
        }

        event->data.touch.absX = libinput_event_touch_get_x_transformed(touch, input_width);
        event->data.touch.absY = libinput_event_touch_get_y_transformed(touch, input_height);
    }

    //    vg_debug() << "libinput_source_t::pop_absolute_event: type: " << libinput_event_get_type(input_event) << " absX, absY : " << event->data.absolute.absX << "," << event->data.absolute.absY;
}

void
libinput_source_t::handle_key_event(struct libinput_event *ev)
{
    xt_input_event event;

    Q_CHECK_PTR(ev);

    memset(&event, 0x00, sizeof(xt_input_event));

    struct libinput_event_keyboard *k = libinput_event_get_keyboard_event(ev);
    Q_CHECK_PTR(k);

    event.type = XT_TYPE_KEY;
    event.flags = libinput_event_keyboard_get_key_state(k);
    event.keyCode = libinput_event_keyboard_get_key(k);

#ifdef TAP_INPUT
    write_event(EV_KEY, event.keyCode, event.flags);
    write_sync_event();
#endif

    emit forward_filtered_input_event(event);
}

void
libinput_source_t::handle_relative_mouse_event(struct libinput_event *ev)
{
    struct libinput_event_pointer *m;
    xt_input_event event;
    Q_CHECK_PTR(ev);

    memset(&event, 0x00, sizeof(xt_input_event));

    m = libinput_event_get_pointer_event(ev);
    Q_CHECK_PTR(m);

    event.data.relative.relX = libinput_event_pointer_get_dx(m);
    event.data.relative.relY = libinput_event_pointer_get_dy(m);

#ifdef TAP_INPUT
    write_event(EV_REL, REL_X, event.data.relative.relX);
    write_event(EV_REL, REL_Y, event.data.relative.relY);
    write_sync_event();
#endif

    // arbitrary
    event.type = XT_TYPE_RELATIVE;

    emit forward_filtered_input_event(event);
}

void
libinput_source_t::handle_absolute_mouse_event(struct libinput_event *ev)
{
    // Create an event that will describe our absolute motion...
    struct xt_input_event event;
    memset(&event, 0x00, sizeof(xt_input_event));
    event.type = XT_TYPE_ABSOLUTE;

    //... populate it with the pointer location.
    populate_absolute_event(&event, ev);

#ifdef TAP_INPUT
    write_event(EV_ABS, ABS_X, event.data.absolute.absX);
    write_event(EV_ABS, ABS_Y, event.data.absolute.absY);
    write_sync_event();
#endif

    //... and generate the relevant movement.
    emit forward_filtered_input_event(event);
}

void
libinput_source_t::handle_mouse_button_event(struct libinput_event *ev)
{
    struct libinput_event_pointer *m;
    xt_input_event event;
    memset(&event, 0x00, sizeof(xt_input_event));
    Q_CHECK_PTR(ev);

    m = libinput_event_get_pointer_event(ev);

    // arbitrary
    event.type = XT_TYPE_RELATIVE;
    event.flags = libinput_event_pointer_get_button_state(m);
    event.keyCode = libinput_event_pointer_get_button(m);

#ifdef TAP_INPUT
    write_event(EV_KEY, event.keyCode, event.flags);
    write_sync_event();
#endif

    emit forward_filtered_input_event(event);
}

void
libinput_source_t::handle_scroll_event(struct libinput_event *ev)
{
    struct libinput_event_pointer *m;
    xt_input_event event;
    Q_CHECK_PTR(ev);

    memset(&event, 0x00, sizeof(xt_input_event));

    m = libinput_event_get_pointer_event(ev);

    // Send a relative event, as scroll events are always a relative motion.
    event.type = XT_TYPE_RELATIVE;

    // If this scroll event represents a mouse-wheel scroll, pass the number of clicks
    // on directly (without libinput processing), as it will be processed by the guest
    if (libinput_event_pointer_get_axis_source(m) == LIBINPUT_POINTER_AXIS_SOURCE_WHEEL) {
        event.data.relative.relZ = libinput_event_pointer_get_axis_value_discrete(m, LIBINPUT_POINTER_AXIS_SCROLL_VERTICAL);

#ifdef TAP_INPUT
        int32_t scroll = libinput_event_pointer_get_axis_value_discrete(m, LIBINPUT_POINTER_AXIS_SCROLL_VERTICAL) < 0 ? -1 : 1;
        write_event(EV_REL, REL_WHEEL, scroll);
        write_sync_event();
#endif
    }

    // Otherwise, libinput has interpreted a touchpad (or other continuous scroll source)
    // for us. In these cases, there's no direct analogue to number of clicks, so we'll
    // send on the processed value.
    else {
        event.data.relative.relZ = touch_scroll_sensitivity * libinput_event_pointer_get_axis_value(m, LIBINPUT_POINTER_AXIS_SCROLL_VERTICAL);
    }

    emit forward_filtered_input_event(event);
}

/**
 * Send the absolute movement corresponding to a given touch event.
 * Can be used for general movement, or before/after click events.
 *
 * @param ev The libinput event to be parsed.
 */
void
libinput_source_t::send_absolute_movement(struct libinput_event *ev)
{
    Q_CHECK_PTR(ev);

    // Create an event that will describe our absolute motion...
    struct xt_input_event event;
    memset(&event, 0x00, sizeof(xt_input_event));
    event.type = XT_TYPE_ABSOLUTE;

    //... and populate it with the touch point's details.
    this->populate_absolute_event(&event, ev);

    //... and generate the relevant movement.
    emit forward_filtered_input_event(event);
}

/**
 *
 */
void
libinput_source_t::send_multitouch_event(struct libinput_event *ev, uint32_t event_type)
{
    Q_CHECK_PTR(ev);

    std::shared_ptr<vm_input_t> vm = NULL;

    // Get a reference to the libinput object describing the given event.
    struct libinput_event_touch *touch = libinput_event_get_touch_event(ev);
    Q_CHECK_PTR(touch);

    // Try to get a description of the touch event slot-- which identifies
    // have to filter on specific types for the slot call according to libinput.h
    libinput_event_type evt = libinput_event_get_type(ev);
    int32_t slot = 0;

    switch (evt) {
        case LIBINPUT_EVENT_TOUCH_DOWN:
        case LIBINPUT_EVENT_TOUCH_UP:
        case LIBINPUT_EVENT_TOUCH_MOTION:
        case LIBINPUT_EVENT_TOUCH_CANCEL:
            slot = libinput_event_touch_get_slot(touch);
            break;
        default:
            slot = 0;
    }

    // create the internal xt_event
    struct xt_input_event xt_event;
    memset(&xt_event, 0x00, sizeof(xt_input_event));
    xt_event.type = event_type;
    xt_event.flags = slot;           //old
    xt_event.data.touch.slot = slot; //new

    switch (xt_event.type) {
        case XT_TYPE_TOUCH_DOWN:
        case XT_TYPE_TOUCH_MOVE:
            //Translation #1 for libinput, get it the point in reference to our parent input plane.
            //The second translation is in the pv_vm_input.cpp's transform_event method.
            xt_event.data.touch.absX = libinput_event_touch_get_x_transformed(touch, m_wm.input_plane()->rect().width());
            xt_event.data.touch.absY = libinput_event_touch_get_y_transformed(touch, m_wm.input_plane()->rect().height());
            break;
        default:
            break;
    }

    emit forward_filtered_input_event(xt_event);
}

void
libinput_source_t::handle_multitouch_event(struct libinput_event *ev)
{
    //FIXME clean this up with a map ( so we dont have to keep calling get_type )
    //acts as a filter as we add messages
    switch (libinput_event_get_type(ev)) {
        case LIBINPUT_EVENT_TOUCH_DOWN:
            send_multitouch_event(ev, XT_TYPE_TOUCH_DOWN);
            break;
        case LIBINPUT_EVENT_TOUCH_UP:
            send_multitouch_event(ev, XT_TYPE_TOUCH_UP);
            break;
        case LIBINPUT_EVENT_TOUCH_MOTION:
            send_multitouch_event(ev, XT_TYPE_TOUCH_MOVE);
            break;
        case LIBINPUT_EVENT_TOUCH_CANCEL:
            send_multitouch_event(ev, XT_TYPE_TOUCH_CANCEL);
            break;
        case LIBINPUT_EVENT_TOUCH_FRAME:
            send_multitouch_event(ev, XT_TYPE_TOUCH_FRAME);
            break;
        default:
            break;
    }
}

void
libinput_source_t::handle_tablet_tool_event(struct libinput_event *ev)
{
    struct libinput_event_tablet_tool *pev = libinput_event_get_tablet_tool_event(ev);
    Q_CHECK_PTR(pev);
    auto event_type = libinput_event_get_type(ev);
    xt_input_event event;
    memset(&event, 0x00, sizeof(xt_input_event));

    event.data.tablet_tool.tool_type = libinput_tablet_tool_get_type(libinput_event_tablet_tool_get_tool(pev)) == LIBINPUT_TABLET_TOOL_TYPE_PEN ? XT_TYPE_TOOL_PEN : XT_TYPE_TOOL_ERASER;
    event.data.tablet_tool.pressure = libinput_event_tablet_tool_get_pressure(pev) * XT_TYPE_TOOL_PRESSUREMAX;
    event.data.tablet_tool.absX = libinput_event_tablet_tool_get_x_transformed(pev, m_wm.input_plane()->rect().width());
    event.data.tablet_tool.absY = libinput_event_tablet_tool_get_y_transformed(pev, m_wm.input_plane()->rect().height());

    switch (event_type) {
        case LIBINPUT_EVENT_TABLET_TOOL_TIP:
            event.type = XT_TYPE_TOOL_TIP;
            event.data.tablet_tool.flags |= libinput_event_tablet_tool_get_tip_state(pev) == LIBINPUT_TABLET_TOOL_TIP_DOWN ? XT_TYPE_TOOL_FLAG_TIP : 0;
            break;
        case LIBINPUT_EVENT_TABLET_TOOL_AXIS:
            event.type = XT_TYPE_TOOL_AXIS;
            break;
        case LIBINPUT_EVENT_TABLET_TOOL_BUTTON:
            event.type = XT_TYPE_TOOL_BUTTON;
            event.data.tablet_tool.flags = libinput_event_tablet_tool_get_button_state(pev) == LIBINPUT_BUTTON_STATE_PRESSED ? XT_TYPE_TOOL_FLAG_BUTTONPRESSED : 0;
            event.data.tablet_tool.button = libinput_event_tablet_tool_get_button(pev);
            break;
        case LIBINPUT_EVENT_TABLET_TOOL_PROXIMITY:
            event.type = XT_TYPE_TOOL_PROXIMITY;
            event.data.tablet_tool.flags = libinput_event_tablet_tool_get_proximity_state(pev) == LIBINPUT_TABLET_TOOL_PROXIMITY_STATE_IN ? XT_TYPE_TOOL_FLAG_PROXIMITY : 0;
            break;
        default:
            event.type = XT_TYPE_NULL;
            vg_debug() << "Unknown tablet tool event.";
            break;
    }

    if (event.type != XT_TYPE_NULL) {
        emit forward_filtered_input_event(event);
    }
}

void
libinput_source_t::set_mouse_speed(float mouse_speed)
{
    for (auto &dev : m_devices) {
        if ((libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_POINTER) != 0) &&
            (libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_GESTURE) == 0)) {
            libinput_device_config_accel_set_speed((libinput_device *) dev, mouse_speed);
        }
    }
}

void
libinput_source_t::set_touchpad_tap_to_click(bool enabled)
{
    for (auto &dev : m_devices) {
        if ((libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_POINTER) != 0) &&
            (libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_GESTURE) != 0)) {
            libinput_device_config_tap_set_enabled((libinput_device *) dev, enabled ? LIBINPUT_CONFIG_TAP_ENABLED : LIBINPUT_CONFIG_TAP_DISABLED);
        }
    }
}

void
libinput_source_t::set_touchpad_scrolling(bool enabled)
{
    for (auto &dev : m_devices) {
        if ((libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_POINTER) != 0) &&
            (libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_GESTURE) != 0)) {
            libinput_device_config_scroll_set_method((libinput_device *) dev, enabled ? libinput_device_config_scroll_get_default_method((libinput_device *) dev) : LIBINPUT_CONFIG_SCROLL_NO_SCROLL);
        }
    }
}

void
libinput_source_t::set_touchpad_speed(float touchpad_speed)
{
    for (auto &dev : m_devices) {
        if ((libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_POINTER) != 0) &&
            (libinput_device_has_capability((libinput_device *) dev, LIBINPUT_DEVICE_CAP_GESTURE) != 0)) {
            libinput_device_config_accel_set_speed((libinput_device *) dev, touchpad_speed);
        }
    }
}

#ifdef TAP_INPUT
void
libinput_source_t::set_tap_input(bool enabled)
{
    tap_input_enabled = enabled;
}

void
libinput_source_t::clear_tap_input()
{
    // Don't clear the file if tap input isn't disabled first
    if (tap_input_enabled) {
        qWarning() << "Recorded input cannot be cleared while record is still enabled.";
        return;
    }

    ftruncate(m_tap_fd, 0);
}
#else
void
libinput_source_t::set_tap_input(bool enabled)
{
    (void) enabled;
    return;
}

void
libinput_source_t::clear_tap_input()
{
    return;
}
#endif
