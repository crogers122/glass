QT += core gui

include(../../common_include.prx)
include(../../backend_include.prx)
include(../../xenstore_include.prx)
include(../../drm_include.prx)

TARGET = glass
TEMPLATE = app

CONFIG += c++14

SOURCES += glass.cpp
HEADERS += ../../common/include/logging.h

#Implementation specifics
INCLUDEPATH += "../../window_manager/fs_window_manager"
INCLUDEPATH += "../../renderer/common/sources"
INCLUDEPATH += "../../renderer/common/sinks"
INCLUDEPATH += "../../renderer/dumb_renderer"
INCLUDEPATH += "../../input/input_server"
INCLUDEPATH += "../../toolstack/xenmgr"
INCLUDEPATH += "../.."

LIBS += -L$$VG_BASE_DIR/lib -lvginput -ldumb_renderer -lfs_wm -lxenmgr_toolstack -livc -lpvbackendhelper

vnc_renderer {
	DEFINES += VNC_RENDERER
	LIBS += -lvnc_renderer
	message(VNC selected as the default renderer.)
}

target.path = /usr/bin
INSTALLS += target
