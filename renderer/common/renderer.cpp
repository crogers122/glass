//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "renderer.h"
#include "sort_config.h"

#include <QCursor>
#include <QFontDatabase>
#include <QTimer>
#include <fstream>
extern "C" {
#include <unistd.h>
}
int32_t g_banner_height = 25;
uint32_t renderer_t::m_frame_time = 20;

renderer_t::renderer_t(window_manager_t &wm) : m_wm(wm),
                                               m_background_image("/storage/glass/background.jpg"),
                                               m_timer(nullptr),
                                               m_ready(false),
                                               m_cloned(false),
                                               m_screenblanking_state(false)
{
    // Root privileges is needed to get the code to work
    if (geteuid() != 0) {
        qCritical() << "This executable must be run with root privileges!!!";
        std::exit(1);
    }

    QObject::connect(&m_vglass_dbus_proxy, &vglass_dbus_proxy::config_changed, this, &renderer_t::config_changed);

    QObject::connect(&m_vglass_dbus_proxy, &vglass_dbus_proxy::identify_on, this, &renderer_t::identify_on);
    QObject::connect(&m_vglass_dbus_proxy, &vglass_dbus_proxy::identify_off, this, &renderer_t::identify_off);

    QObject::connect(&m_vglass_dbus_proxy, &vglass_dbus_proxy::dpms_on, this, &renderer_t::dpms_on);
    QObject::connect(&m_vglass_dbus_proxy, &vglass_dbus_proxy::dpms_off, this, &renderer_t::dpms_off);

    QObject::connect(this, &renderer_t::render_complete, &m_vglass_dbus_proxy, &vglass_dbus_proxy::render_complete);

    QObject::connect(&m_wm, &window_manager_t::render_signal, this, &renderer_t::render_slot);

    while (!m_ready) {
        sleep(1);
        QCoreApplication::processEvents();
    }
    QFontDatabase::addApplicationFont("/usr/share/fonts/truetype/DejaVuSans-Bold.ttf");
}

renderer_t::renderer_t(window_manager_t &wm,
                       std::string display_config_path) : m_wm(wm),
                                                          m_background_image("/storage/glass/background.jpg"),
                                                          m_timer(nullptr),
                                                          m_ready(false),
                                                          m_screenblanking_state(false)
{
    // Root privileges is needed to get the code to work
    if (geteuid() != 0) {
        qCritical() << "This executable must be run with root privileges!!!";
        std::exit(1);
    }

    QFontDatabase::addApplicationFont("/usr/lib/fonts/DejaVuSans.ttf");

    std::ifstream i(display_config_path, std::ifstream::in);
    json config;
    i >> config;

    setup_gpus(config);
}

renderer_t::renderer_t(window_manager_t &wm,
                       json &display_config) : m_wm(wm),
                                               m_background_image("/storage/glass/background.jpg"),
                                               m_timer(nullptr),
                                               m_ready(false),
                                               m_screenblanking_state(false)
{
    // Root privileges is needed to get the code to work
    if (geteuid() != 0) {
        qCritical() << "This executable must be run with root privileges!!!";
        std::exit(1);
    }

    QFontDatabase::addApplicationFont("/usr/lib/fonts/DejaVuSans.ttf");

    setup_gpus(display_config);
}

uint32_t
renderer_t::frame_time()
{
    return m_frame_time;
}

json
public_get_config(std::list<std::string> devices)
{
    json config;

    for (auto device : devices) {
        json gpu_config;
        std::string device_path = "/dev/dri/card" + device;
        drm_gpu_t(gpu_config, device_path);
        config["gpus"].push_back(gpu_config);
    }

    config["frame_time"] = renderer_t::frame_time();
    config["banner_height"] = g_banner_height;

    return config;
}

void
renderer_t::setup_gpus(json &display_config)
{
    if (!m_timer) {
        m_timer = new QTimer(this);
        m_timer->setSingleShot(true);
        connect(m_timer, SIGNAL(timeout()), this, SLOT(render()));
    } else {
        m_timer->stop();
    }

    int banner_height = display_config["banner_height"];

    for (auto &gpu_conf : display_config["gpus"]) {
        std::string desktop_type = gpu_conf["desktop_type"];
        if (desktop_type == "dedicated") {
            m_gpus.push_back(std::move(std::make_unique<pt_gpu_t>(gpu_conf, m_wm.input_plane())));
            auto gpu = m_gpus.back().get();
            if (!gpu) {
                continue;
            }

            // We treat passthrough gpus much like pinned displays
            for (auto uuid : gpu->pinned_desktops().keys()) {
                m_pinned_uuid.push_back(uuid);
                setup_desktop(gpu->pinned_desktops()[uuid]);
            }
        } else {
            try {
                m_gpus.push_back(std::move(std::make_unique<drm_gpu_t>(gpu_conf, banner_height, m_wm.input_plane())));
            } catch (...) {
                sleep(3);
                return config_changed(display_config);
            }
            auto gpu = m_gpus.back().get();
            if (!gpu) {
                continue;
            }

            for (auto uuid : gpu->pinned_desktops().keys()) {
                m_pinned_uuid.push_back(uuid);
                setup_desktop(gpu->pinned_desktops()[uuid]);
            }

            if (gpu->desktop()->mode() == desktop_plane_t::SHARED) {
                setup_desktop(gpu->desktop());
                m_cloned = false;
            } else if (gpu->desktop()->mode() == desktop_plane_t::CLONED) {
                setup_desktop(gpu->desktop());
                m_cloned = true;
            }
        }
    }

    m_wm.reset_global_visibility();

    m_timer->start(m_frame_time);
}

void
renderer_t::reset()
{
    TRACE;

    // Remove the planes and other state associated with
    // the window manager, none of it is valid now that
    // the configuration is changing.
    m_wm.reset();

    for (auto &guest : m_guests) {
        if (!guest) {
            continue;
        }
        guest->update_render_targets(nullptr, guest->uuid());
    }

    // Teardown the gpus, so that we can probe the modes
    // for disman (drmMasterDrop)
    m_gpus.clear();

    // Clear pinning, this can probably later be moved into vm_render_t
    m_pinned_uuid.clear();
}

bool
renderer_t::current_config_matches(json &new_config)
{
    sort_config(new_config);

    return m_config == new_config;
}

void
renderer_t::config_changed(json gpu_config)
{
    TRACE;

    if (current_config_matches(gpu_config)) {
        vg_debug() << "Current config matches - skipping hotplug stuff";

        return;
    }

    m_ready = true;

    g_banner_height = gpu_config["banner_height"];
    m_frame_time = gpu_config["frame_time"];

    reset();

    setup_gpus(gpu_config);

    m_config_changed = true;
    m_config = gpu_config;

    emit hotplug();
}

void
renderer_t::setup_desktop(std::shared_ptr<desktop_plane_t> desktop_plane)
{
    m_wm.add_plane(desktop_plane);
}

void
renderer_t::add_guest(std::shared_ptr<vm_render_t> guest)
{
    if (!guest) {
        throw std::invalid_argument("Null guest in renderer.");
    }

    m_guests[guest->uuid()] = guest;
    QObject::connect(guest.get(), &vm_render_t::show_cursor_signal, this, &renderer_t::show_cursor);
    QObject::connect(guest.get(), &vm_render_t::move_cursor_signal, this, &renderer_t::move_guest_cursor);
    QObject::connect(guest.get(), &vm_render_t::hide_cursor_signal, this, &renderer_t::hide_cursor);
    QObject::connect(guest.get(), &vm_render_t::render_signal, this, &renderer_t::render_slot);
    QObject::connect(&m_wm, &window_manager_t::focus_changed, this, &renderer_t::render_slot_uuid);
}

void
renderer_t::remove_guest(std::shared_ptr<vm_t> vm)
{
    if (!vm) {
        return;
    }

    for (auto desktop : m_wm.input_plane()->desktops()) {
        if (!desktop) {
            continue;
        }
        desktop->remove_guest(vm->uuid());
    }

    m_guests.remove(vm->uuid());
}

void
renderer_t::dpms_display_on(display_plane_t &display)
{
    display.dpms_on();
}

void
renderer_t::dpms_display_off(display_plane_t &display)
{
    display.dpms_off();
}

void
renderer_t::dpms_on()
{
    m_screenblanking_state = true;

    for (auto &desktop : m_wm.input_plane()->desktops()) {
        if (desktop && desktop->renderable()) {
            for (auto &display : desktop->displays()) {
                if (!display) {
                    continue;
                }

                dpms_display_on(*display);
            }
        }
    }
}

void
renderer_t::dpms_off()
{
    if (!m_screenblanking_state) {
        return;
    }

    m_screenblanking_state = false;

    for (auto &desktop : m_wm.input_plane()->desktops()) {
        if (desktop && desktop->renderable()) {
            for (auto &display : desktop->displays()) {
                if (!display) {
                    continue;
                }

                dpms_display_off(*display);
            }
        }
    }
}

void
renderer_t::identify_on(std::string text)
{
    (void) text;
    m_wm.text_overlays_on();
    render_slot();
}

void
renderer_t::identify_off(std::string text)
{
    (void) text;
    m_wm.text_overlays_off();
    render_slot();
}

void
renderer_t::refresh()
{
}

void
renderer_t::save_screenshot()
{
    vg_debug() << "renderer_t::save_screenshot() is not finished.";
    // for(const auto &painter : mPinnedPainters)
    // {
    //     painter->saveScreenshot(ec);
    // }

    // for(const auto &painter : mSharedPainters)
    // {
    //     painter->saveScreenshot(ec);
    // }
}

int
dump_region(region_t region)
{
    vg_info() << region;
    return 0;
}

template <typename Overlay>
void
renderer_t::render_overlays(QPainter &painter, desktop_plane_t *desktop, Overlay overlays, display_plane_t *display_plane, region_t &display_clip, region_t &painted_clip)
{
    for (auto &overlay : overlays) {
        if (!overlay) {
            continue;
        }

        if (overlay->display_id() != display_plane->unique_id()) {
            continue;
        }

        overlay->render(painter, desktop, display_plane, display_clip, painted_clip);
    }
}

bool
renderer_t::render_vm_to_display(QPainter &painter,
                                 uuid_t uuid,
                                 desktop_plane_t *desktop,
                                 display_plane_t *display,
                                 region_t &display_region,
                                 region_t &painted_region)
{
    bool visible_updated = false;
    Expects(desktop);
    Expects(display);

    vm_region_t *vm_region = m_wm.vm_region(uuid);
    vm_render_t *vm_render = m_guests[uuid].get();

    if (!vm_render || !vm_region) {
        return visible_updated;
    }

    render_target_plane_t *render_target = desktop->render_target(uuid, display);

    region_t visible_clip_region = vm_region->create_visible_region(desktop,
                                                                    display,
                                                                    render_target);

    if (!render_target) {
        display->current_clip() += visible_clip_region;
        return visible_updated;
    }

    render_source_plane_t *rsp = render_target->render_source();
    rect_t guest_target = desktop->map_to(display,
                                          render_target->parent_rect());

    region_t clip_region;
    region_t dirty_clip_region = vm_region->create_dirty_region(desktop,
                                                                display,
                                                                render_target);

    if (!vm_region->dirty_updated()) {
        display->current_clip() += visible_clip_region;
        return visible_updated;
    }

    if (!rsp || !rsp->framebuffer() || rsp->framebuffer()->isNull() || !rsp->valid()) {
        painter.setClipping(true);
        painter.setClipRegion(visible_clip_region & guest_target);
        painter.fillRect(guest_target, Qt::black);
        display_region -= guest_target;
        painted_region += guest_target;
        display->current_clip() += visible_clip_region;
        return visible_updated;
    }

    display_region += dirty_clip_region;

    clip_region = display_region & visible_clip_region & dirty_clip_region & guest_target;
    painter.setClipping(true);
    painter.setClipRegion(display_region);
    vm_region->render_overlays(painter,
                               desktop,
                               display,
                               render_target,
                               display_region,
                               painted_region);

    painter.setClipRegion(clip_region & display_region);
    if (vm_region->base()->is_sleeping() || vm_render->blanked()) {
        painter.fillRect(guest_target, Qt::black);
    } else {
        painter.drawImage(guest_target, *rsp->framebuffer(), rsp->rect());
    }

#ifdef DEBUG_CLIPS
    painter.setClipping(false);
    for (auto &rect : dirty_clip_region.rects()) {
        QPen pen(Qt::red);
        pen.setWidth(4);
        painter.setPen(pen);
        painter.drawRect(rect);
    }

    for (auto &rect : visible_clip_region.rects()) {
        QPen pen(Qt::green);
        pen.setWidth(4);
        painter.setPen(pen);
        painter.drawRect(rect);
    }
#endif

    display_region -= clip_region;
    painted_region += clip_region;
    display->current_clip() += visible_clip_region;

    return visible_updated;
}

void
renderer_t::base_render_display(desktop_plane_t *desktop, display_plane_t *display_plane, list_t<uuid_t> &focus_stack)
{
    Expects(display_plane);
    QPainter framebuffer(display_plane->framebuffer()->image().get());
    region_t &display_clip = display_plane->clip();
    display_plane->dirty_bitmap() &= region_t();

    framebuffer.setFont(QFont("DejaVu Sans"));

    framebuffer.setClipping(true);
    framebuffer.setClipRegion(display_clip);

    // Rely on the window manager providing the correct visibility
    // regions, regardless of focus ordering (top to bottom or vice-versa)
    for (auto &uuid : focus_stack) {
        if (desktop->uuid() != m_wm.desktop_uuid(uuid)) {
            continue;
        }
        render_vm_to_display(framebuffer, uuid, desktop, display_plane, display_clip, display_plane->dirty_bitmap());
    }

    render_overlays(framebuffer, desktop, m_wm.switcher_overlays(), display_plane, display_clip, display_plane->dirty_bitmap());

    render_overlays(framebuffer, desktop, m_wm.text_overlays(), display_plane, display_clip, display_plane->dirty_bitmap());

    render_overlays(framebuffer, desktop, m_wm.banner_overlays(), display_plane, display_clip, display_plane->dirty_bitmap());

    framebuffer.setClipping(true);

    region_t unpainted = display_plane->last_clip() - display_plane->current_clip();
    framebuffer.setClipRegion(unpainted);
    for (auto rect = unpainted.begin();
         rect != unpainted.end();
         rect++) {
#ifdef DEBUG_CLIPS
        QPen pen(Qt::blue);
        pen.setWidth(4);
        framebuffer.setPen(pen);
        framebuffer.drawRect(*rect);
#endif
        framebuffer.fillRect(*rect, QColor(70, 70, 70));
    }

    framebuffer.end();
    display_plane->framebuffer()->flush(unpainted);

    display_plane->last_clip() = display_plane->current_clip();
    display_plane->current_clip() = region_t();

    if (0) {
        dump_region(region_t());
    }
}

void
renderer_t::render_display(desktop_plane_t *desktop, display_plane_t *display_plane, list_t<uuid_t> &focus_stack)
{
    pre_render_display(desktop, display_plane);
    base_render_display(desktop, display_plane, focus_stack);
    post_render_display(desktop, display_plane);
}

void
renderer_t::render_desktop(desktop_plane_t *desktop, list_t<uuid_t> &focus_stack)
{
    if (!desktop || !desktop->renderable()) {
        return;
    }

    //Lock desktop against mode changes while rendering
    QMutexLocker locker(desktop->lock());
    for (auto &display_plane : desktop->displays()) {
        if (!display_plane || !display_plane->scratch()) {
            continue;
        }

        render_display(desktop, display_plane.get(), focus_stack);
    }
}

void
renderer_t::render()
{
    auto focus_stack = m_wm.render_focus_stack();
    for (auto desktop : m_wm.input_plane()->desktops()) {
        if (!desktop) {
            continue;
        }

        render_desktop(desktop.get(), focus_stack);
    }
    m_wm.reset_global_visibility();

    if (m_config_changed) {
        emit render_complete(true);
        m_config_changed = false;
    }
}

void
renderer_t::render_slot()
{
    if (m_timer->isActive()) {
        return;
    }

    m_timer->start(m_frame_time);

    render();
}

void
renderer_t::render_slot_uuid(const uuid_t &uuid)
{
    (void) uuid;
    render_slot();
}

desktop_plane_t *
renderer_t::desktop(uuid_t uuid)
{
    desktop_plane_t *desktop = m_wm.input_plane()->desktops()[m_wm.desktop_uuid(uuid)].get();

    return desktop;
}

void
renderer_t::move_guest_cursor(uuid_t uuid, window_key_t key, point_t point)
{
    m_wm.move_cursor_from_guest(uuid, key, point);
}

void
renderer_t::show_cursor(uuid_t uuid, uint32_t key, point_t point, std::shared_ptr<cursor_t> cursor)
{

    auto desktop = m_wm.input_plane()->desktops()[m_wm.desktop_uuid(uuid)];
    if (!desktop || !desktop->renderable() || uuid != m_wm.render_focus_stack().back()) {
        return;
    }

    auto display = desktop->current_display();

    if (!display || desktop->render_target(uuid, key) != desktop->render_target(uuid, display)) {
        return;
    }

    for (auto &d : desktop->displays()) {
        if (d && d.get() != display) {
            d->hide_cursor();
        }
    }

    auto rsp = desktop->render_source(uuid, key);
    if (rsp != nullptr && rsp->cursor()) {
        rsp->cursor()->show_cursor();
    }

    cursor->set_hotspot(point);
    display->show_cursor(cursor);
}

void
renderer_t::hide_cursor(uuid_t uuid, uint32_t key)
{

    auto desktop = m_wm.input_plane()->desktops()[m_wm.desktop_uuid(uuid)];
    if (!desktop || !desktop->renderable() || uuid != m_wm.render_focus_stack().back()) {
        return;
    }

    render_target_plane_t *rtp = desktop->render_target(uuid, key);
    if (!rtp) {
        return;
    }

    auto rsp = rtp->render_source();
    if (!rsp) {
        return;
    }

    if (rsp != nullptr && rsp->cursor()) {
        rsp->cursor()->hide_cursor();
    }

    point_t rtp_point = rtp->map_from(rsp, point_t(1, 1));
    point_t desktop_point = desktop->map_from(rtp, rtp_point);

    auto display = desktop->display(desktop_point);

    if (!display) {
        return;
    }

    display->hide_cursor();
}
