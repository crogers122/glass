QT       += core gui 

include(../../../common_include.prx)
include(../../../drm_include.prx)

TARGET = test
TEMPLATE = app

CONFIG += c++14

SOURCES += main.cpp
SOURCES += drm_gpu.cpp
SOURCES += drm_crtc.cpp
SOURCES += drm_display.cpp
SOURCES += drm_connector.cpp
#SOURCES += connectorgroup.cpp
SOURCES += drm_mode.cpp
SOURCES += dumb_fb.cpp

HEADERS += drm_gpu.h
HEADERS += drm_crtc.h 
HEADERS += drm_display.h 
HEADERS += drm_connector.h
#HEADERS += connectorgroup.h
HEADERS += drm_mode.h
HEADERS += dumb_fb.h
HEADERS += ../../include/framebuffer.h
HEADERS += ../../include/cursor.h

INCLUDEPATH += "./"
