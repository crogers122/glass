//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "drm_connector.h"

#include <exception>
#include <iostream>

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <QDebug>
#include <QMap>

#include <display.h>

typedef struct {
    int connector_type;
    const char *connector_name;
} connector_msg_t;

static connector_msg_t connector_msg_table[] = {
    {DRM_MODE_CONNECTOR_Unknown, "Unknown"},
    {DRM_MODE_CONNECTOR_VGA, "VGA"},
    {DRM_MODE_CONNECTOR_DVII, "DVII"},
    {DRM_MODE_CONNECTOR_DVID, "DVID"},
    {DRM_MODE_CONNECTOR_DVIA, "DVIA"},
    {DRM_MODE_CONNECTOR_Composite, "Composite"},
    {DRM_MODE_CONNECTOR_SVIDEO, "SVIDEO"},
    {DRM_MODE_CONNECTOR_LVDS, "LVDS"},
    {DRM_MODE_CONNECTOR_Component, "Component"},
    {DRM_MODE_CONNECTOR_9PinDIN, "9PinDIN"},
    {DRM_MODE_CONNECTOR_DisplayPort, "DisplayPort"},
    {DRM_MODE_CONNECTOR_HDMIA, "HDMIA"},
    {DRM_MODE_CONNECTOR_HDMIB, "HDMIB"},
    {DRM_MODE_CONNECTOR_TV, "TV"},
    {DRM_MODE_CONNECTOR_eDP, "eDP"},
    {DRM_MODE_CONNECTOR_VIRTUAL, "VIRTUAL"},
    {DRM_MODE_CONNECTOR_DSI, "DSI"},
};

#define CONNECTOR_TABLE_LEN sizeof(connector_msg_table) / sizeof(connector_msg_table[0])

static const char *
connector_name(int connector_type)
{
    for (size_t i = 0; i < CONNECTOR_TABLE_LEN; i++) {
        if (connector_msg_table[i].connector_type == connector_type) {
            return connector_msg_table[i].connector_name;
        }
    }

    return NULL;
}

drm_connector_t::drm_connector_t(int32_t fd,
                                 std::unique_ptr<drmModeConnector, void (*)(drmModeConnector *)> connector)
    : m_drm_fd(fd),
      m_connector(std::move(connector)),
      m_dpms(false),
      m_panel_fitter(false)
{
    Expects(m_connector != nullptr);
    Expects(fd > 0);

    auto modes_lookup = QMap<QString, std::shared_ptr<drm_mode_t>>();

    // If the connector has a property called "scaling mode", it means that
    // it supports panel fitting. If it supports panel fitting, it means that
    // you can set the display to whatever resolution you want, and the
    // displays internal panel fitter will scale the display for you. If this
    // is the case, we want to use this instead of using software scaling when
    // resolutions don't match. To turn on panel fitting, you need to set the
    // scaling mode to DRM_MODE_SCALE_FULLSCREEN
    if ((m_panel_fitter = has_property("scaling mode")) == true) {
        set_property("scaling mode", DRM_MODE_SCALE_FULLSCREEN);
    }

    m_max_mode = nullptr;
    m_preferred_mode = nullptr;
    m_modes.clear();

    if (!m_connector) {
        throw std::exception();
    }

    // Unfortunately can't do the fancy C++14/GSL stuff with the connector modes,
    // because DRM exhibits ownership of these as part of a large object. Taking
    // the address of a member of a span in an iterator always returns the first
    // (last?)
    // element of the span.
    for (int i = 0; i < m_connector->count_modes; i++) {
        auto mode = std::make_shared<drm_mode_t>(&m_connector->modes[i]);
        auto name = mode->name(true);
        auto raw_name = mode->name(false);

        if (!modes_lookup.contains(name)) {
            m_modes.push_back(mode);
            modes_lookup[name] = mode;
        } else {
            auto old_mode = modes_lookup[name];

            if (raw_name == "preferred") {
                m_modes.removeAll(old_mode);
                m_modes.push_back(mode);
                modes_lookup[name] = mode;
            }

            if (old_mode->refresh_rate() == 60) {
                continue;
            }

            if (mode->refresh_rate() == 60 || mode->refresh_rate() > old_mode->refresh_rate()) {
                m_modes.removeAll(old_mode);
                m_modes.push_back(mode);
                modes_lookup[name] = mode;
            }
        }
    }

    for (const auto &mode : m_modes) {
        if (!m_max_mode || *mode > *m_max_mode) {
            m_max_mode = mode;
            m_preferred_mode = mode;
        }
    }

    for (const auto &mode : m_modes) {
        if (mode->name(false) == "preferred") {
            m_preferred_mode = mode;
        }
    }

    return;
}

drm_connector_t::~drm_connector_t() {}

QString
drm_connector_t::name() const
{
    return QString(connector_name(m_connector->connector_type)) +
           QString::number(m_connector->connector_type_id);
}

uint32_t
drm_connector_t::id(void) const
{
    if (!m_connector) {
        return 0;
    }

    return m_connector->connector_id;
}

bool
drm_connector_t::is_connected(void) const
{
    if (!m_connector)
        return false;

    return (m_connector->connector_id != 0 && m_connector->connection == DRM_MODE_CONNECTED &&
            m_connector->count_modes > 0);
}

bool
drm_connector_t::panel_fitter(void) const
{
    return m_panel_fitter;
}

std::shared_ptr<drm_mode_t>
drm_connector_t::max_mode(void) const
{
    return m_max_mode;
}

std::shared_ptr<drm_mode_t>
drm_connector_t::preferred_mode(void) const
{
    return m_preferred_mode;
}

QList<std::shared_ptr<drm_mode_t>>
drm_connector_t::modes(void) const
{
    return m_modes;
}

std::shared_ptr<drm_mode_t>
drm_connector_t::mode(const QSize &resolution)
{
    for (const auto &mode : m_modes) {
        if (!mode) {
            continue;
        }

        if (resolution == mode->resolution()) {
            return mode;
        }
    }

    return NULL;
}

bool
drm_connector_t::dpms(void) const
{
    return has_property("DPMS");
}

void
drm_connector_t::dpms_on()
{
    set_property("DPMS", DRM_MODE_DPMS_OFF);

    return;
}

void
drm_connector_t::dpms_off()
{
    set_property("DPMS", DRM_MODE_DPMS_ON);

    return;
}

drmModeConnector *
drm_connector_t::connector(void) const
{
    return m_connector.get();
}

bool
drm_connector_t::has_property(const QString &name) const
{
    return (property_id(name) != 0);
}

uint32_t
drm_connector_t::property_id(const QString &name) const
{
    if (!m_connector) {
        return 0;
    }

    for (auto i = 0; i < m_connector->count_props; i++) {
        std::unique_ptr<drmModePropertyRes, void (*)(drmModePropertyRes *)> drm_property_resource(
            drmModeGetProperty(m_drm_fd, m_connector->props[i]), drmModeFreeProperty);

        if (!drm_property_resource) {
            continue;
        }

        if (drm_property_resource->name == name) {
            return drm_property_resource->prop_id;
        }
    }

    return 0;
}

void
dump_edid(drmModePropertyBlobPtr blob_ptr)
{
    uint32_t i = 0;
    for (unsigned char *j = (unsigned char *) blob_ptr->data; i < blob_ptr->length; i++, j++) {
        std::cout << QString::number(*j, 16).toStdString() << " ";
        if (i % 16 == 0) {
            std::cout << '\n';
        }
    }
}

#define SHARED_MASK 0x7FFFFFFF;

uint32_t
drm_connector_t::get_edid_hash()
{
    // If we have the ability to fetch edid data,
    // make a hash for this connector with
    // edid + connector name
    if (has_property("EDID") && is_connected()) {
        get_edid();

        if (m_edid) {
            QByteArray edid_data = QByteArray(reinterpret_cast<const char *>(m_edid->data), m_edid->length);

            edid_data.append(name().toUtf8());

            return qHash(edid_data) & SHARED_MASK;
        } else {
            qDebug() << "Failed to get edid for connector...";
            return qHash(name()) & SHARED_MASK;
        }
    } else {
        // Otherwise the best we can do is just
        // use the connector name.
        return qHash(name()) & SHARED_MASK;
    }
}

void
drm_connector_t::get_edid()
{

    for (auto i = 0; i < m_connector->count_props; i++) {
        std::unique_ptr<drmModePropertyRes, void (*)(drmModePropertyRes *)> drm_property_resource(drmModeGetProperty(m_drm_fd, m_connector->props[i]), drmModeFreeProperty);
        if (QString(drm_property_resource->name) != "EDID") {
            continue;
        }

        m_edid = std::shared_ptr<drmModePropertyBlobRes>(drmModeGetPropertyBlob(m_drm_fd, m_connector->prop_values[i]), drmModeFreePropertyBlob);

        if (!m_edid) {
            qDebug() << "Failed to retrieve edid blob";
            return;
        }
    }
}

void
drm_connector_t::set_property(const QString &name, uint64_t value)
{
    auto pid = property_id(name);
    Expects(pid != 0);

    if (drmModeConnectorSetProperty(m_drm_fd, id(), pid, value)) {
        auto error = errno;
        qDebug() << "Failed: drmModeConnectorSetProperty:  name =" << name << " value =" << value
                 << " error =" << strerror(error);
    }

    return;
}
