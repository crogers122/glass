//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DRM_CONNECTOR__H
#define DRM_CONNECTOR__H

#include "drm_mode.h"
#include <QList>

#include <gsl/gsl>

class drm_connector_t
{
public:
    drm_connector_t(int32_t fd, std::unique_ptr<drmModeConnector, void (*)(drmModeConnector *)> connector);
    virtual ~drm_connector_t();

    virtual QString name(void) const;
    virtual uint32_t id(void) const;

    virtual bool is_connected(void) const;
    virtual bool panel_fitter(void) const;

    virtual uint32_t get_edid_hash();

    virtual std::shared_ptr<drm_mode_t> max_mode(void) const;
    virtual std::shared_ptr<drm_mode_t> preferred_mode(void) const;
    virtual QList<std::shared_ptr<drm_mode_t>> modes(void) const;

    virtual std::shared_ptr<drm_mode_t> mode(const QSize &resolution);

    virtual bool dpms(void) const;
    virtual void dpms_on();
    virtual void dpms_off();

    virtual drmModeConnector *connector(void) const;

    bool has_property(const QString &name) const;
    uint32_t property_id(const QString &name) const;
    void set_property(const QString &name, uint64_t value);
    void get_edid();

    void set_crtc_id(uint32_t crtc_id) { m_crtc_id = crtc_id; }
    uint32_t crtc_id() { return m_crtc_id; }

private:
    int32_t m_drm_fd;
    uint32_t m_crtc_id;
    std::unique_ptr<drmModeConnector, void (*)(drmModeConnector *)> m_connector;
    std::shared_ptr<drmModePropertyBlobRes> m_edid;
    bool m_dpms;
    bool m_panel_fitter;

    std::shared_ptr<drm_mode_t> m_max_mode;
    std::shared_ptr<drm_mode_t> m_preferred_mode;
    QList<std::shared_ptr<drm_mode_t>> m_modes;
};

inline uint
qHash(std::shared_ptr<drm_connector_t> key, uint seed = 0)
{
    uintptr_t ptr = (uintptr_t) key.get();
    uint k = (uint)(ptr & ((uint32_t) -1));

    return k ^ seed;
}

#endif // CONNECTOR__H
