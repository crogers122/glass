//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "drm_gpu.h"
#include "sort_config.h"
#include <algorithm>

uint
qHash(QSize key)
{
    return qHash(qHash(key.width()) ^ qHash(key.height()) ^ 0x95782);
}

// Populates an empty json object with the current configurations available
// for the GPU in device_path, and its associated monitors and connectors.
// This object cannot be used for rendering.
drm_gpu_t::drm_gpu_t(json &config, std::string &device_path, std::shared_ptr<plane_t> desktop_parent) : m_mode_resource(nullptr, drmModeFreeResources), m_desktop_parent(desktop_parent)
{
    TRACE;
    m_drm_fd = open(device_path.c_str(), O_RDWR);
    m_fd_close = m_drm_fd;

    Expects(m_drm_fd > 0);

    // Obtain the mode resources available for the card
    m_mode_resource = std::shared_ptr<drmModeRes>(drmModeGetResources(m_drm_fd), drmModeFreeResources);

    // Setup resource spans
    m_crtc_span = gsl::span<uint32_t>(m_mode_resource->crtcs, m_mode_resource->count_crtcs);
    m_connector_span = gsl::span<uint32_t>(m_mode_resource->connectors, m_mode_resource->count_connectors);
    m_encoder_span = gsl::span<uint32_t>(m_mode_resource->encoders, m_mode_resource->count_encoders);

    create_connectors();
    create_crtcs();
    create_encoders();

    std::shared_ptr<drmVersion> version(drmGetVersion(m_drm_fd), drmFreeVersion);

    config["device_path"] = device_path;
    config["desktop_type"] = "shared";
    config["name"] = version->name;
    config["renderable"] = true;
    std::string config_hash;
    int x_count = 0;
    for (auto connector : m_connectors.values()) {
        json e_display;
        e_display["name"] = connector->name().toStdString();
        e_display["x"] = x_count;
        e_display["y"] = 0;
        e_display["uuid"] = "none";
        e_display["preferred_mode_index"] = 0;
        e_display["edid_hash"] = QString::number(connector->get_edid_hash(), 16).toStdString();

        for (auto mode : connector->modes()) {
            if (mode->resolution_width() >= 800 && mode->resolution_height() >= 600) {
                json e_mode;

                e_mode["name"] = mode->name(true).toStdString();
                e_mode["width"] = mode->resolution_width();
                e_mode["height"] = mode->resolution_height();
                e_display["modes"].push_back(e_mode);
            }
        }

        if (!e_display["modes"].is_null()) {
            int x_stride = e_display["modes"][0]["width"];
            x_count += x_stride;
            config["displays"].push_back(e_display);
        }
    }

    // DRM enumeration isn't ordered, so to create consistant hashes, sort
    // the displays by edid_hash value.
    std::sort(config["displays"].begin(), config["displays"].end(), sort_displays);

    for (auto i_display : config["displays"]) {
        config_hash += i_display["name"];
        config_hash += i_display["edid_hash"];
    }

    config["config_hash"] = QString::number(qHash(QString::fromStdString(config_hash)), 16).toStdString();
}

drm_gpu_t::drm_gpu_t(json &gpu_config, int banner_height, std::shared_ptr<plane_t> desktop_parent) : gpu_t(gpu_config),
                                                                                                     m_banner_height(banner_height),
                                                                                                     m_mode_resource(nullptr, drmModeFreeResources),
                                                                                                     m_desktop_parent(desktop_parent)
{
    TRACE;
    int rc = 0;
    const std::string device_path = gpu_config["device_path"];
    const std::string desktop_type = gpu_config["desktop_type"];

    m_drm_fd = open(device_path.c_str(), O_RDWR);
    m_fd_close = m_drm_fd;

    Expects(m_drm_fd > 0);

    // Take master control of the DRM device
    rc = drmSetMaster(m_drm_fd);
    if (rc) {
        throw std::runtime_error("Failed to set DRM master");
    }

    // Obtain the mode resources available for the card
    m_mode_resource = std::shared_ptr<drmModeRes>(drmModeGetResources(m_drm_fd), drmModeFreeResources);

    // Setup resource spans
    m_crtc_span = gsl::span<uint32_t>(m_mode_resource->crtcs, m_mode_resource->count_crtcs);
    m_connector_span = gsl::span<uint32_t>(m_mode_resource->connectors, m_mode_resource->count_connectors);
    m_encoder_span = gsl::span<uint32_t>(m_mode_resource->encoders, m_mode_resource->count_encoders);

    create_connectors();
    create_crtcs();
    create_encoders();

    if (desktop_type == "cloned") {
        create_cloned_desktop(gpu_config["displays"]);
    } else if (desktop_type == "shared") {
        create_shared_desktop(gpu_config["displays"]);
    }

    // There can only be one desktop type. The pinned monitors are
    // not considered part of that desktop, they are a set of desktops
    // handled individually.
    setup_pinned_monitors(gpu_config["displays"]);

    if (pinned_desktops().size()) {
        vg_debug() << "Pinned desktop exists";
        vg_debug() << pinned_desktops().size();
        for (auto &pinned : pinned_desktops()) {
            if (!pinned) {
                continue;
            }

            vg_debug() << pinned->origin() << pinned->rect();
        }
    }

    if (m_desktop) {
        if (m_desktop->mode() == desktop_plane_t::SHARED) {
            vg_debug() << "Shared desktop exists";
        } else if (m_desktop->mode() == desktop_plane_t::CLONED) {
            vg_debug() << "Cloned desktop exists.";
        }
        vg_debug() << m_desktop->origin() << m_desktop->rect();
    }
}

drm_gpu_t::~drm_gpu_t()
{
    TRACE;
    m_desktop = nullptr;
    m_desktop_parent = nullptr;
}

void
drm_gpu_t::create_shared_desktop(json &displays)
{
    m_desktop = std::make_shared<desktop_plane_t>(uuid_t(),
                                                  rect_t(0, 0, 0, 0),
                                                  point_t(0, 0),
                                                  true,
                                                  desktop_plane_t::SHARED,
                                                  m_desktop_parent);
    setup_shared_monitors(displays);

    m_desktop->lock()->lock();
    for (auto display_plane : m_shared_display_planes) {
        m_desktop->add_display(display_plane);
    }

    m_desktop->reorigin_displays();
    m_desktop->translate_planes();
    m_desktop->lock()->unlock();
}

void
drm_gpu_t::create_cloned_desktop(json &displays)
{
    m_desktop = std::make_shared<desktop_plane_t>(uuid_t(),
                                                  rect_t(0, 0, 0, 0),
                                                  point_t(0, 0),
                                                  true,
                                                  desktop_plane_t::CLONED,
                                                  m_desktop_parent);
    setup_cloned_monitors(displays);

    m_desktop->lock()->lock();
    for (auto display_plane : m_cloned_display_planes) {
        m_desktop->add_display(display_plane);
        break;
    }

    m_desktop->reorigin_displays();
    m_desktop->translate_planes();
    m_desktop->lock()->unlock();
}

qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>>
drm_gpu_t::pinned_desktops()
{
    if (m_pinned_display_planes.keys().size() == 0) {
        return m_pinned_desktops;
    }

    for (auto uuid : m_pinned_display_planes.keys()) {
        if (m_pinned_display_planes[uuid].size() == 0) {
            continue;
        }

        if (!m_pinned_desktops[uuid]) {
            vg_debug() << "Creating a pinned desktop: " << uuid;
            auto desktop_plane = std::make_shared<desktop_plane_t>(uuid,
                                                                   rect_t(0, 0, 0, 0),
                                                                   point_t(0, 0),
                                                                   true,
                                                                   desktop_plane_t::PINNED,
                                                                   m_desktop_parent);

            if (!desktop_plane) {
                return m_pinned_desktops;
            }
            desktop_plane->lock()->lock();
            for (auto display_plane : m_pinned_display_planes[uuid]) {
                desktop_plane->add_display(display_plane);
            }

            desktop_plane->reorigin_displays();
            desktop_plane->translate_planes();
            desktop_plane->lock()->unlock();
            m_pinned_desktops[uuid] = desktop_plane;
        }
    }

    return m_pinned_desktops;
}

void
drm_gpu_t::disable_display(display_plane_t &display)
{
    (void) display;
}

void
drm_gpu_t::enable_display(display_plane_t &display)
{
    (void) display;
}

void
drm_gpu_t::show_cursor(display_plane_t &display, std::shared_ptr<cursor_t> cursor)
{
    display.show_cursor(cursor);
}

void
drm_gpu_t::move_cursor(display_plane_t &display, point_t point)
{
    display.move_cursor(point);
}

void
drm_gpu_t::hide_cursor(display_plane_t &display)
{
    display.hide_cursor();
}

void
drm_gpu_t::associate_crtcs_to_connectors(const qlist_t<std::shared_ptr<drm_connector_t>> &connectors,
                                         qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> &crtcid_lookup,
                                         qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> &connector_lookup)
{
    // Find a suitable crtc
    for (auto connector : connectors) {
        gsl::span<uint32_t> encoder_span(connector->connector()->encoders,
                                         connector->connector()->count_encoders);

        for (auto encoder_id : encoder_span) {
            if (!m_encoders_available.contains(encoder_id)) {
                continue;
            }

            if (crtcid_lookup.keys().contains(connector)) {
                continue;
            }

            std::unique_ptr<drmModeEncoder, void (*)(drmModeEncoder *)> encoder(
                drmModeGetEncoder(m_drm_fd, encoder_id), drmModeFreeEncoder);

            for (auto crtc_id : m_crtc_span) {
                if (!(encoder->possible_crtcs & m_crtcs[crtc_id]->bitmask())) {
                    continue;
                }

                if (!m_crtcs_available.contains(crtc_id)) {
                    continue;
                }

                m_encoders_available.removeAll(encoder_id);
                m_crtcs_available.removeAll(crtc_id);

                crtcid_lookup[connector] = crtc_id;
                connector->set_crtc_id(crtc_id);
                connector_lookup[crtc_id] = connector;
                break;
            }

            if (crtcid_lookup[connector] == 0) {
                qDebug() << "Failed to find a suitable CRTC";
                return;
            }
        }
    }

    return;
}

void
drm_gpu_t::crtc_connector_join(std::shared_ptr<drm_connector_t> connector,
                               uint32_t crtc_id,
                               point_t origin,
                               QSize resolution,
                               std::shared_ptr<dumb_fb_t> pfb,
                               std::shared_ptr<dumb_fb_t> pcursor)
{
    std::shared_ptr<dumb_fb_t> fb;
    std::shared_ptr<dumb_fb_t> cursor;
    if (!pfb) {
        fb = std::make_shared<dumb_fb_t>(m_drm_fd, resolution);
    } else {
        fb = pfb;
    }

    if (!pcursor) {
        cursor = std::make_shared<dumb_fb_t>(m_drm_fd, QSize(64, 64), QImage::Format_ARGB32);
    } else {
        cursor = pcursor;
    }

    m_crtcs[crtc_id]->add_fb(fb);
    m_crtcs[crtc_id]->add_cursor(cursor);
    m_crtcs[crtc_id]->add_connector(connector);
    m_crtcs[crtc_id]->set_resolution(connector, resolution);
    m_crtcs[crtc_id]->set_position(origin);
    m_crtcs[crtc_id]->set_unique_id(connector->get_edid_hash());
}

QSize
drm_gpu_t::get_cloned_resolution(json &displays)
{
    QSize resolution(-1, -1);
    qset_t<QSize> available_modes;

    bool panel_fitter_available = false;
    for (auto connector : m_connectors) {
        if (connector->panel_fitter()) {
            panel_fitter_available = true;
        }
    }

    // Find the compatible connectors in the configuration
    for (auto monitor : displays) {
        int preferred_mode_index = monitor["preferred_mode_index"];
        std::string connector_name = monitor["name"];

        // Find compatible connectors

        for (auto connector : m_connectors) {
            qset_t<QSize> connector_modes;
            if (m_cloned_connectors.contains(connector)) {
                continue;
            }

            if (connector->name() == QString(connector_name.c_str())) {
                m_cloned_connectors.append(connector);
                if ((!connector->panel_fitter() && panel_fitter_available) || connector->modes().size() > 1) {
                    for (auto mode : monitor["modes"]) {
                        connector_modes.insert(QSize(mode["width"],
                                                     mode["height"]));
                    }

                    if (available_modes.isEmpty()) {
                        available_modes += connector_modes;
                    } else {
                        available_modes &= connector_modes;
                    }

                    QSize preferred_mode = QSize(monitor["modes"][preferred_mode_index]["width"],
                                                 monitor["modes"][preferred_mode_index]["height"]);
                    if (available_modes.contains(preferred_mode)) {
                        resolution = preferred_mode;
                    } else {
                        resolution = QSize(-1, -1);
                    }
                } else {
                    vg_info() << "WARNING: Skipped connector" << connector->name();
                }
            }
        }
    }

    if (resolution == QSize(-1, -1)) {
        QSize max_compatible_mode(1024, 768);
        for (auto tmp : available_modes) {
            if ((tmp.width() * tmp.height()) > (max_compatible_mode.width() * max_compatible_mode.height())) {
                max_compatible_mode = tmp;
            }
        }

        resolution = max_compatible_mode;
    }

    return resolution;
}

void
drm_gpu_t::setup_cloned_monitors(json &displays)
{
    QSize resolution(-1, -1);
    point_t origin(0, 0);

    if (displays.size() == 1) {
        setup_shared_monitors(displays);
        return;
    }

    resolution = get_cloned_resolution(displays);

    std::shared_ptr<dumb_fb_t> fb = std::make_shared<dumb_fb_t>(m_drm_fd, resolution);
    std::shared_ptr<dumb_fb_t> cursor = std::make_shared<dumb_fb_t>(m_drm_fd, QSize(64, 64), QImage::Format_ARGB32);

    // Setup lookup and reverse lookup
    qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> crtcid_lookup;
    qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> connector_lookup;

    associate_crtcs_to_connectors(m_cloned_connectors, crtcid_lookup, connector_lookup);

    std::shared_ptr<drm_connector_t> panel_fit_connector = nullptr;
    std::shared_ptr<drm_crtc_t> crtc = nullptr;

    if (m_cloned_connectors.size() == 1) {
        vg_debug() << "Only one connector, cloned doesn't make a whole lot of sense";
        auto connector = m_cloned_connectors.front();
        if (connector->is_connected() && connector->mode(resolution)) {
            crtc_connector_join(connector,
                                crtcid_lookup[connector],
                                origin,
                                resolution,
                                fb,
                                cursor);
            add_cloned_display(m_crtcs[crtcid_lookup[connector]]);
            return;
        }
    }

    for (auto connector : connector_lookup.values()) {
        if (connector->is_connected() && connector->panel_fitter()) {
            panel_fit_connector = connector;
            break;
        }
    }

    for (auto connector : connector_lookup.values()) {
        if (connector->is_connected()) {
            crtc = m_crtcs[crtcid_lookup[connector]];
            break;
        }
    }

    uint32_t cloned_edid = 0;

    for (auto connector : connector_lookup.values()) {
        if (!connector->mode(resolution)) {
            continue;
        }

        cloned_edid ^= connector->get_edid_hash();
    }

    if (panel_fit_connector) {
        if ((resolution.width() == 3840 && resolution.height() == 2160) &&
            !panel_fit_connector->mode(resolution)) {
            // Panel fitting 4k to a non-4k laptop monitor produces poor results
            resolution = QSize(1920, 1080);
        }

        crtc->add_fb(fb);
        crtc->add_cursor(cursor);
        crtc->set_unique_id(cloned_edid);
        crtc->set_position(origin);

        std::shared_ptr<drm_connector_t> tmp_connector = nullptr;

        for (auto connector : connector_lookup.values()) {
            if (connector->is_connected() && connector != panel_fit_connector) {
                if (!connector->mode(resolution)) {
                    continue;
                }

                crtc->add_connector(connector);
                crtc->set_resolution(connector, connector->mode(resolution)->modeinfo());
                tmp_connector = connector;
            }
        }

        crtc->add_connector(panel_fit_connector);
        crtc->set_resolution(panel_fit_connector, tmp_connector->mode(resolution)->modeinfo());
        add_cloned_display(crtc);
    } else {
        crtc->add_fb(fb);
        crtc->add_cursor(cursor);
        crtc->set_unique_id(cloned_edid);
        crtc->set_position(origin);
        for (auto connector : connector_lookup.values()) {
            if (connector->is_connected() && connector != panel_fit_connector) {
                if (!connector->mode(resolution)) {
                    continue;
                }

                crtc->add_connector(connector);
                crtc->set_resolution(connector, connector->mode(resolution)->modeinfo());
            }
        }
        add_cloned_display(crtc);
    }

    // Software cloned mode... yuck
    if (!panel_fit_connector && !crtc && m_cloned_display_planes.size() == 0) {
        fb = nullptr;
        cursor = nullptr;
        for (auto connector : connector_lookup.values()) {
            if (connector->is_connected() && !connector->panel_fitter() && m_cloned_connectors.size() >= 1) {
                crtc_connector_join(connector,
                                    crtcid_lookup[connector],
                                    origin,
                                    resolution,
                                    nullptr,
                                    nullptr);
                add_cloned_display(m_crtcs[crtcid_lookup[connector]]);
            }
        }
    }
}

void
drm_gpu_t::setup_shared_monitors(json &displays)
{
    qmap_t<std::shared_ptr<drm_connector_t>, QSize> connector_resolution_lookup;
    qmap_t<std::shared_ptr<drm_connector_t>, point_t> connector_origin_lookup;
    for (auto monitor : displays) {
        std::string uuid = monitor["uuid"];
        if (uuid != "none") {
            continue;
        }

        int preferred_mode_index = monitor["preferred_mode_index"];
        std::string name = monitor["name"];
        QSize resolution(monitor["modes"][preferred_mode_index]["width"],
                         monitor["modes"][preferred_mode_index]["height"]);
        point_t origin(monitor["x"], monitor["y"]);

        for (auto connector : m_connectors) {
            if (connector->name().toStdString() == name && connector->mode(resolution)) {
                connector_resolution_lookup[connector] = resolution;
                connector_origin_lookup[connector] = origin;
                m_shared_connectors.append(connector);
            }
        }
    }

    // Setup lookup and reverse lookup
    qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> crtcid_lookup;
    qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> connector_lookup;

    associate_crtcs_to_connectors(m_shared_connectors, crtcid_lookup, connector_lookup);

    for (auto connector : connector_lookup.values()) {
        if (connector->is_connected() && connector->mode(connector_resolution_lookup[connector])) {
            crtc_connector_join(connector,
                                crtcid_lookup[connector],
                                connector_origin_lookup[connector],
                                connector_resolution_lookup[connector],
                                nullptr,
                                nullptr);
            add_shared_display(m_crtcs[crtcid_lookup[connector]]);
        }
    }
}

void
drm_gpu_t::setup_pinned_monitors(json &displays)
{
    qmap_t<std::shared_ptr<drm_connector_t>, QSize> connector_resolution_lookup;
    qmap_t<std::shared_ptr<drm_connector_t>, point_t> connector_origin_lookup;

    for (auto monitor : displays) {
        std::string uuid = monitor["uuid"];
        if (uuid == "none") {
            continue;
        }

        int preferred_mode_index = monitor["preferred_mode_index"];
        std::string name = monitor["name"];
        QSize resolution(monitor["modes"][preferred_mode_index]["width"],
                         monitor["modes"][preferred_mode_index]["height"]);
        point_t origin(monitor["x"], monitor["y"]);

        for (auto connector : m_connectors) {
            if (connector->name().toStdString() == name && connector->mode(resolution)) {
                connector_resolution_lookup[connector] = resolution;
                connector_origin_lookup[connector] = origin;
                m_pinned_connectors[uuid_t(QString::fromStdString(uuid))].push_back(connector);
            }
        }
    }

    for (auto uuid : m_pinned_connectors.keys()) {
        qmap_t<std::shared_ptr<drm_connector_t>, uint32_t> crtcid_lookup;
        qmap_t<uint32_t, std::shared_ptr<drm_connector_t>> connector_lookup;
        qlist_t<std::shared_ptr<display_plane_t>> display_planes;

        associate_crtcs_to_connectors(m_pinned_connectors[uuid], crtcid_lookup, connector_lookup);

        for (auto connector : connector_lookup.values()) {
            if (connector->is_connected() && connector->mode(connector_resolution_lookup[connector])) {
                crtc_connector_join(connector,
                                    crtcid_lookup[connector],
                                    connector_origin_lookup[connector],
                                    connector_resolution_lookup[connector],
                                    nullptr,
                                    nullptr);
                display_planes.push_back(m_crtcs[crtcid_lookup[connector]]);
                m_crtcs[crtcid_lookup[connector]]->set_parent(m_desktop_parent);
            }
        }
        m_pinned_display_planes.insert(uuid, display_planes);
    }
}

void
drm_gpu_t::add_cloned_display(std::shared_ptr<drm_crtc_t> crtc)
{
    crtc->set_name("CLONED");
    m_cloned_display_planes.push_back(crtc);
    crtc->set_parent(m_desktop);
}

void
drm_gpu_t::add_shared_display(std::shared_ptr<drm_crtc_t> crtc)
{
    m_shared_display_planes.push_back(crtc);
    crtc->set_parent(m_desktop);
}

void
drm_gpu_t::create_connectors()
{
    for (auto connector_id : m_connector_span) {
        drmModeConnectorPtr p = drmModeGetConnector(m_drm_fd, connector_id);
        if (p == NULL) {
            /* We just skip connector_ids that are NULL... */
            continue;
        }

        std::unique_ptr<drmModeConnector, void (*)(drmModeConnector *)> connector(
            p, drmModeFreeConnector);
        m_connectors[connector_id] = std::make_shared<drm_connector_t>(m_drm_fd, std::move(connector));
        m_connectors_available.append(connector_id);
    }
}

void
drm_gpu_t::create_crtcs()
{
    // Best to distinguish by CRTC first, and narrow the scope of available
    // configurations from there (it reduces the set of configurations in the
    // quickest manner).
    for (auto crtc_id : m_crtc_span) {
        // Before we create the CRTC and by extension create the dumb buffer,
        // distinguish what the available connectors for this particular crtc_id.
        // We'll pass that information to the CRTC in a span that contains the
        // connector_ids to use.
        m_crtcs[crtc_id] = std::make_shared<drm_crtc_t>(m_drm_fd, crtc_id, m_mode_resource, m_banner_height);
        m_crtcs_available.append(crtc_id);
    }
}

void
drm_gpu_t::create_encoders()
{
    for (auto encoder_id : m_encoder_span) {
        m_encoders_available.append(encoder_id);
    }
}

void
drm_gpu_t::apply_to_crtcs(std::function<void(int, uint32_t)> fn)
{
    for (auto crtc_id : m_crtc_span) {
        fn(m_drm_fd, crtc_id);
    }
}

void
drm_gpu_t::apply_to_connectors(std::function<void(int, uint32_t)> fn)
{
    for (auto connector_id : m_connector_span) {
        fn(m_drm_fd, connector_id);
    }
}

void
drm_gpu_t::apply_to_encoders(std::function<void(int, uint32_t)> fn)
{
    for (auto encoder_id : m_encoder_span) {
        fn(m_drm_fd, encoder_id);
    }
}

void
drm_gpu_t::apply_to_modes(uint32_t conn_id, std::function<void(uint32_t)> fn)
{
    (void) conn_id;
    (void) fn;
}
