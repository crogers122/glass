#include "sort_config.h"

bool
sort_displays(json &l, json &r)
{
    return l["edid_hash"] < r["edid_hash"];
}

bool
sort_modes(json &l, json &r)
{
    int l_h = l["height"];
    int r_h = r["height"];
    int l_w = l["width"];
    int r_w = r["width"];
    bool ret;

    // Use > to order largest first
    if (l_h == r_h) {
        ret = l_w > r_w;
    } else {
        ret = l_h > r_h;
    }

    return ret;
}

// The complex types for displays and modes need to be explicitly ordered since
// they don't compare reliably otherwise.
void
sort_config(json &config)
{
    for (auto &gpu_conf : config["gpus"]) {
        for (auto &display_conf : gpu_conf["displays"]) {
            std::sort(display_conf["modes"].begin(),
                      display_conf["modes"].end(),
                      sort_modes);
        }

        std::sort(gpu_conf["displays"].begin(),
                  gpu_conf["displays"].end(),
                  sort_displays);
    }
}
