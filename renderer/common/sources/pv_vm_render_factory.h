//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PV_VM_RENDER_FACTORY__H
#define PV_VM_RENDER_FACTORY__H

#include <glass_types.h>

#include <QObject>

#include "pv_guest/pv_vm_render.h"
#include <vm_render_factory.h>

class pv_vm_render_factory_t : public QObject, public vm_render_factory_t
{
    Q_OBJECT;

public:
    explicit pv_vm_render_factory_t(renderer_t &renderer);
    virtual ~pv_vm_render_factory_t() = default;
    virtual std::shared_ptr<vm_render_t> make_vm_render(toolstack_t &toolstack, std::shared_ptr<vm_base_t> vm);

signals:
    void add_guest(std::shared_ptr<vm_render_t> vm);
};

#endif // PV_VM_RENDER_FACTORY__H
