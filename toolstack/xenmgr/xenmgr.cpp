//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <gsl/gsl>
#include <xenmgr.h>

#ifdef TEST
#define expects(x)                    \
    if (!x) {                         \
        throw std::runtime_error(""); \
    }
#else
#define expects(x) Expects(x)
#endif

xenmgr_t::xenmgr_t(std::shared_ptr<vm_render_factory_t> vm_render_factory,
                   std::shared_ptr<vm_region_factory_t> vm_region_factory,
                   std::shared_ptr<vm_input_factory_t> vm_input_factory) : toolstack_t(vm_render_factory, vm_region_factory, vm_input_factory),
                                                                           m_vm_render_factory(vm_render_factory),
                                                                           m_vm_region_factory(vm_region_factory),
                                                                           m_vm_input_factory(vm_input_factory)

{

    // Setup dbus_listener for interacting and getting important signals
    m_dbus_listener = make_dbus_listener();

    seam::qobj_connect(m_dbus_listener.get(), SIGNAL(guest_started(const uuid_t &)), this, SLOT(guest_started(const uuid_t &)));
    seam::qobj_connect(m_dbus_listener.get(), SIGNAL(guest_stopped(const uuid_t &)), this, SLOT(guest_stopped(const uuid_t &)));
    seam::qobj_connect(m_dbus_listener.get(), SIGNAL(guest_deleted(const uuid_t &)), this, SLOT(guest_stopped(const uuid_t &)));
    seam::qobj_connect(m_dbus_listener.get(), SIGNAL(guest_rebooted(const uuid_t &)), this, SLOT(guest_rebooted(const uuid_t &)));
    seam::qobj_connect(m_dbus_listener.get(), SIGNAL(guest_slept(const uuid_t &)), this, SLOT(guest_slept(const uuid_t &)));
    seam::qobj_connect(m_dbus_listener.get(), SIGNAL(guest_name_changed(const uuid_t &)), this, SLOT(guest_name_changed(const uuid_t &)));
    seam::qobj_connect(m_dbus_listener.get(), SIGNAL(battery_charge_state_changed(uint)), this, SLOT(battery_charge_state_changed(uint)));
    seam::qobj_connect(m_dbus_listener.get(), SIGNAL(battery_percentage_changed(uint)), this, SLOT(battery_percentage_changed(uint)));
    seam::qobj_connect(this, SIGNAL(refresh_battery()), m_dbus_listener.get(), SLOT(refresh_battery()));

    // Get the current state of the toolstack and VMs on the system
    process_vm_list(m_dbus_listener->get_vm_list());
}

std::shared_ptr<vm_t>
xenmgr_t::find_guest(const uuid_t uuid)
{
    if (m_vms.contains(uuid)) {
        return m_vms[uuid];
    }

    throw std::out_of_range("Uuid not in hash table");
}

std::shared_ptr<vm_t>
xenmgr_t::find_guest(const domid_t domid)
{
    for (auto vm : m_vms) {
        if (vm->domid() == domid) {
            return vm;
        }
    }

    throw std::out_of_range("No vm with given domid");
}

void
xenmgr_t::reset()
{
    process_vm_list(m_dbus_listener->get_vm_list());
}

void
xenmgr_t::guest_started(const uuid_t &uuid)
{
    expects(!uuid.isNull());
    create_guest(uuid);

    if (m_vms.contains(uuid)) {
        m_vms[uuid]->base()->set_sleep_state(false);
        emit wake_up_guest(uuid);
    }
}

void
xenmgr_t::guest_slept(const uuid_t &uuid)
{
    expects(!uuid.isNull());

    if (m_vms.contains(uuid)) {
        m_vms[uuid]->base()->set_sleep_state(true);
    }
    emit sleep_guest(uuid);
}

void
xenmgr_t::guest_stopped(const uuid_t &uuid)
{
    expects(!uuid.isNull());
    destroy_guest(uuid);
}

void
xenmgr_t::guest_rebooted(const uuid_t &uuid)
{
    expects(!uuid.isNull());
    emit reboot_guest(uuid);
}

void
xenmgr_t::guest_name_changed(const uuid_t &uuid)
{
    expects(!uuid.isNull());
    if (m_vms.contains(uuid)) {
        emit guest_name_changed(m_vms[uuid]);
    }
}

void
xenmgr_t::battery_charge_state_changed(uint state)
{
    emit battery_charge_state_change(state);
}

void
xenmgr_t::battery_percentage_changed(uint percentage)
{
    emit battery_percentage_change(percentage);
}

void
xenmgr_t::battery_refresh()
{
    emit refresh_battery();
}

bool
xenmgr_t::get_display_configurable()
{
    return m_dbus_listener->get_display_configurable();
}

bool
xenmgr_t::get_laptop()
{
    return m_dbus_listener->get_laptop();
}

bool
xenmgr_t::get_display_power_indicator()
{
    return m_dbus_listener->get_display_power_indicator();
}

domid_t
xenmgr_t::get_domid(uuid_t uuid)
{
    return m_dbus_listener->get_domid(uuid);
}

domid_t
xenmgr_t::get_stub_domid(uuid_t uuid)
{
    return m_dbus_listener->get_stub_domid(uuid);
}

std::string
xenmgr_t::get_vm_name(uuid_t uuid)
{
    return m_dbus_listener->get_name(uuid).toStdString();
}

std::string
xenmgr_t::get_os(uuid_t uuid)
{
    return m_dbus_listener->get_os(uuid).toStdString();
}

std::string
xenmgr_t::get_image_path(uuid_t uuid)
{
    return m_dbus_listener->get_image_path(uuid).toStdString();
}

std::string
xenmgr_t::get_long_form(uuid_t uuid)
{
    return m_dbus_listener->get_long_form(uuid).toStdString();
}

std::string
xenmgr_t::get_short_form(uuid_t uuid)
{
    return m_dbus_listener->get_short_form(uuid).toStdString();
}

std::string
xenmgr_t::get_text_color(uuid_t uuid)
{
    return m_dbus_listener->get_text_color(uuid).toStdString();
}

std::string
xenmgr_t::get_gpu(uuid_t uuid)
{
    return m_dbus_listener->get_gpu(uuid).toStdString();
}

int
xenmgr_t::get_border_width(uuid_t uuid)
{
    return m_dbus_listener->get_border_width(uuid);
}

int
xenmgr_t::get_border_height(uuid_t uuid)
{
    return m_dbus_listener->get_border_height(uuid);
}

int
xenmgr_t::get_slot(uuid_t uuid)
{
    return m_dbus_listener->get_slot(uuid);
}

std::string
xenmgr_t::get_primary_domain_color(uuid_t uuid)
{
    return m_dbus_listener->get_primary_domain_color(uuid).toStdString();
}

std::string
xenmgr_t::get_secondary_domain_color(uuid_t uuid)
{
    return m_dbus_listener->get_secondary_domain_color(uuid).toStdString();
}

std::shared_ptr<vm_base_t>
xenmgr_t::make_vm_base(uuid_t uuid)
{
    std::shared_ptr<vm_base_t> vm_base = std::make_shared<vm_base_t>(m_dbus_listener);

    vm_base->set_uuid(uuid);
    vm_base->set_domid(get_domid(uuid));
    vm_base->set_stub_domid(get_stub_domid(uuid));
    vm_base->set_name(get_vm_name(uuid));
    vm_base->set_os(get_os(uuid));
    vm_base->set_image_path(get_image_path(uuid));
    vm_base->set_long_form(get_long_form(uuid));
    vm_base->set_short_form(get_short_form(uuid));
    vm_base->set_text_color(get_text_color(uuid));
    vm_base->set_gpu(get_gpu(uuid));
    vm_base->set_border_width(get_border_width(uuid));
    vm_base->set_border_height(get_border_height(uuid));
    vm_base->set_slot(get_slot(uuid));
    vm_base->set_primary_domain_color(get_primary_domain_color(uuid));
    vm_base->set_secondary_domain_color(get_secondary_domain_color(uuid));
    vm_base->set_sleep_state(false);

    return vm_base;
}

void
xenmgr_t::create_guest(uuid_t uuid)
{
    if (!m_vms.contains(uuid)) {
        std::shared_ptr<vm_base_t> vm_base = make_vm_base(uuid);
        std::shared_ptr<vm_region_t> vm_region = m_vm_region_factory->make_vm_region(vm_base);
        std::shared_ptr<vm_render_t> vm_render = m_vm_render_factory->make_vm_render(*this, vm_base);
        std::shared_ptr<vm_input_t> vm_input = m_vm_input_factory->make_vm_input(vm_base);

        std::unique_ptr<vm_t> vm = std::make_unique<vm_t>(vm_base, vm_render, vm_region, vm_input);
        m_vms[uuid] = std::move(vm);

        seam::qobj_connect(vm_region.get(), SIGNAL(update_render_targets(desktop_plane_t *, uuid_t)), vm_render.get(), SLOT(update_render_targets(desktop_plane_t *, uuid_t)));
        seam::qobj_connect(vm_render.get(), SIGNAL(calculate_guest_size(uuid_t)), vm_region.get(), SIGNAL(calculate_guest_size(uuid_t)));
        seam::qobj_connect(vm_render.get(), SIGNAL(restore_qemu_signal()), vm_region.get(), SLOT(restore_qemu()));
        emit add_guest(m_vms[uuid]);
    }
}

void
xenmgr_t::refresh_guest(uuid_t uuid)
{
    if (!m_vms.contains(uuid)) {
        return;
    }

    m_vm_region_factory->refresh_vm_region(uuid);
}

void
xenmgr_t::destroy_guest(uuid_t uuid)
{
    if (m_vms.contains(uuid)) {
        emit remove_guest(m_vms[uuid]);
        m_vms.remove(uuid);
    }
}

void
xenmgr_t::process_vm_list(const qlist_t<uuid_t> running_vm_list)
{
    if (!running_vm_list.empty()) {
        for (const auto &uuid : running_vm_list) {
            expects(!uuid.isNull());

            if (!m_vms.contains(uuid)) {
                create_guest(uuid);
            } else {
                refresh_guest(uuid);
            }
        }
    }
}
